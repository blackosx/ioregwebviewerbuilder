#!/bin/sh

# IORegWebViewerBuilder script
# v0.63
#
# This script runs a modified version of Apple's ioreg tool to dump each 
# IORegistry plane to file. Each file is then read, edited where necessary
# and split in to the correct format for viewing with the IORegFileViewer 
# in a web browser.
#
# December 2012 -> June 2013
# Blackosx.
#

# =======================================================================================
# INITIALISATION
# =======================================================================================

# ---------------------------------------------------------------------------------------
Initialise()
{
    local cdir="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    cd "$cdir"; cd ..; 
    
    # GLOBAL VARS

    gWorkingPath="/tmp"
    gWorkingDataPath="${gWorkingPath}/dataFiles"
    gResourcesDir="$( pwd )"/Resources
    gToolsDir="${gResourcesDir}/tools"
    gAssetsDir="${gResourcesDir}/assets"
    gMasterHtmlViewerFileName="IORegFileViewer.html"
    gMasterHtmlViewerFile="${gResourcesDir}"/$gMasterHtmlViewerFileName
    gFinalDir="$HOME/Desktop/IORegWebViewer"
    ioregwv="${gToolsDir}/ioregwv"
    execCD="${gToolsDir}"/cocoaDialog.app/Contents/MacOS/CocoaDialog
}

# =================================================================
# FUNCTIONS
# =================================================================

# ---------------------------------------------------------------------------------------
MoveFinalFiles()
{    
    if [ -d "${gFinalDir}" ]; then
        mv "${gFinalDir}" "${gFinalDir}_$( date -j +%F-%Hh_%M_%S )"
    fi
    
    mkdir -p "${gFinalDir}"/Resources
    if [ -d "${gFinalDir}" ]; then

        # Add created files
        cp -R "${gWorkingDataPath}" "${gFinalDir}"/Resources
        
        # Add resource files.  
        cp "${gMasterHtmlViewerFile}" "${gFinalDir}"
        cp -R "${gResourcesDir}/assets" "${gFinalDir}"/Resources
        cp -R "${gResourcesDir}/scripts" "${gFinalDir}"/Resources
        cp -R "${gResourcesDir}/styles" "${gFinalDir}"/Resources
        cp -R "${gResourcesDir}/jquery_ui_themes" "${gFinalDir}"/Resources
        
        # Clean up
        if [ -d "${gWorkingDataPath}" ]; then
            rm -rf "${gWorkingDataPath}"
        fi
    fi        
}

# =================================================================
# Main
# =================================================================

Initialise

"$ioregwv" -lw0 -pIOService
"$ioregwv" -lw0 -pIOACPIPlane
"$ioregwv" -lw0 -pIODeviceTree
"$ioregwv" -lw0 -pIOPower
"$ioregwv" -lw0 -pIOUSB

MoveFinalFiles

# Remove backup file created by the sed command above.
if [ -f "${gFinalDir}/${gMasterHtmlViewerFileName}"e ]; then
    rm "${gFinalDir}/${gMasterHtmlViewerFileName}"e
fi

# Open web viewer.
open "${gFinalDir}"/"$gMasterHtmlViewerFileName"

